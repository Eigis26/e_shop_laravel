# E-shop Laravel

## Description
This is online shop application in development mode
## Launch
1. Install by cloning: type `git clone git@gitlab.com:Eigis26/e_shop_laravel.git  `in `htdocs` folder;
2. Go to mySQL workbench and choose root connection;
3. Create database named `shop` ;
4. Open project and launch composer command (if globally: `php composer install`, if localy: `php <path to composer.phar> install`);
5. Create  `.env` file by copying `.env.example` content with command line `cp .env.example .env`;
6. In `.env.` file replace DATABASE=laravel to DATABASE=shop';
7. Run command `npm install`;
8. Run command `npm run build`;
9. Migrate into database by typing in command line `php artisan:migrate`;
10. Write into command line `php artisan serve` to start local server;
11. In web browser write `http://127.0.0.1:8000` and push enter
## Admin and User roles
After registration go to you database and find table `users` then in user type insert `1-for admin role` and  `0-for user role`;
## Payments
Disabled for confidetial information but it working well.;
## Email verifaction
Email verifaction disabled for confidential information but it working well.;
## Author
Eigirdas Anciūnas
