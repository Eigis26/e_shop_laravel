<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AdminController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [HomeController::class, 'index']);

    Route::middleware(['auth::sanctum', 'verified'])->get('/dashboard', function(){
        return view('dashboard');
    })->name('dashboard');

    Route::get('/redirect', [HomeController::class, 'redirect']);
    // ->middleware('auth','verified') ----- disabled email verifaction for easer testing
    Route::get('/product_details/{id}', [HomeController::class, 'product_details']);
    Route::get('/show_cart', [HomeController::class, 'show_cart']);
    Route::get('/show_order', [HomeController::class, 'show_order']);
    Route::get('/products', [HomeController::class, 'products']);
    Route::get('/cancel_order/{id}', [HomeController::class, 'cancel_order']);
    Route::post('/add_cart/{id}', [HomeController::class, 'add_cart']);
    Route::get('/remove_cart/{id}', [HomeController::class, 'remove_cart']);
    Route::get('/order/{totalprice}', [HomeController::class, 'order']);
    Route::post('stripe/{totalprice}', [HomeController::class, 'stripePost'])->name('stripe.post');
    Route::get('/view_category', [AdminController::class, 'view_category']);
    Route::post('/add_category', [AdminController::class, 'add_category']);
    Route::get('/delete_category/{id}', [AdminController::class, 'delete_category']);
    Route::get('/view_product', [AdminController::class, 'view_product']);
    Route::post('/add_product', [AdminController::class, 'add_product']);
    Route::get('/show_product', [AdminController::class, 'show_product']);
    Route::get('/delete_product/{id}', [AdminController::class, 'delete_product']);
    Route::get('/upd_product/{id}', [AdminController::class, 'upd_product']);
    Route::post('/upd_product_confirm/{id}', [AdminController::class, 'upd_product_confirm']);
    Route::get('/orders', [AdminController::class, 'orders']);
    Route::get('/delivered/{id}', [AdminController::class, 'delivered']);
    Route::get('/print_pdf/{id}', [AdminController::class, 'print_pdf']);
    Route::get('/search', [AdminController::class, 'search']);