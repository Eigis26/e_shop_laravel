<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function view_category()
    {
        if(Auth::id())
        {
            $data = Category::all();
            return view('admin.category', compact('data'));
        }
        else
        {
            return redirect('login');
        }

    }
    public function add_category(Request $request)
    {
        if(Auth::id())
        {
            $data=new Category;
            $data->category_name=$request->category;
            $data->save();
            return redirect()->back()->with('message', 'Category Added Successfuly');
        }
        else
        {
            return redirect('login');
        }

    }
    public function delete_category($id)
    {
        if(Auth::id())
        {
            $data= Category::find($id);
            $data->delete();
            return redirect()->back()->with('message', 'Category Deleted Successfuly');
        }
        else
        {
            return redirect('login');
        }

    }

    public function view_product()
    {
        if(Auth::id())
        {
            $category = Category::all();
        return view('admin.product', compact('category'));
        }
        else
        {
            return redirect('login');
        }
        
    }

    public function add_product(Request $request)
    {
        if(Auth::id())
        {
            $product=new Product;
            $product->title=$request->title;
            $product->description=$request->description;
            $product->price=$request->price;
            $product->quantity=$request->quantity;
            $product->discount_price=$request->dis_price;
            $product->category=$request->category;
            $image=$request->image;
            $imagename=time().'.'.$image->getClientOriginalExtension();
            $request->image->move('product', $imagename);
            $product->image=$imagename;
            $product->save();
            return redirect()->back()->with('message', 'Product Added Successfuly');
        }
        else
        {
            return redirect('login');
        }

    }

    public function show_product()
    {
        if(Auth::id())
        {
            $product=product::all();
            return view('admin.show_product', compact('product'));
        }
        else
        {
            return redirect('login');
        }

    }

    public function delete_product($id)
    {
        if(Auth::id())
        {
            $data= Product::find($id);
            $data->delete();
            return redirect()->back()->with('message', 'Product Deleted Successfuly');
        }
        else
        {
            return redirect('login');
        }

     }

    public function upd_product($id)
    {
        if(Auth::id())
        {
            $product=Product::find($id);
            $category=Category::all();
    
            return view('admin.upd_product', compact('product', 'category'));
        }
        else
        {
            return redirect('login');
        }

    }

    public function upd_product_confirm(Request $request, $id)
    {
        if(Auth::id())
        {
            $product=product::find($id);
            $product->title=$request->title;
            $product->description=$request->description;
            $product->price=$request->price;
            $product->quantity=$request->quantity;
            $product->discount_price=$request->dis_price;
            $product->category=$request->category;
            $image=$request->image;
            if($image)
            {
                $imagename=time().'.'.$image->getClientOriginalExtension();
                $request->image->move('product', $imagename);
                $product->image=$imagename;
            }
    
            $product->save();
            return redirect()->back()->with('message', 'Product Updated Successfuly');
        }
        else
        {
            return redirect('login');
        }

    }

    public function orders()
    {
        if(Auth::id())
        {
            $order=Order::all();

            return view('admin.orders', compact('order'));
        }
        else
        {
            return redirect('login');
        }

    }
    public function delivered($id)
    {
        if(Auth::id())
        {
            $order=order::find($id);
            $order->delivery_status="Delivered";
            $order->save();
            return redirect()->back();
        }
        else
        {
            return redirect('login');
        }

    }
    public function print_pdf($id)
    {
        if(Auth::id())
        {
            $order=order::find($id);
            $pdf=Pdf::loadView('admin.pdf', compact('order'));
            return $pdf->download('order_details.pdf');
        }
        else
        {
            return redirect('login');
        }

    }
    public function search(Request $request)
    {
        if(Auth::id())
        {
            $searchText=$request->search;
            $order=order::where('name','LIKE',"%$searchText%")->orWhere('phone','LIKE',"%$searchText%")
            ->orWhere('email','LIKE',"%$searchText%")->orWhere('price','LIKE',"%$searchText%")
            ->orWhere('product_title','LIKE',"%$searchText%");
    
            return view('admin.orders',compact('order'));
        }
        else
        {
            return redirect('login');
        }

    }
}
