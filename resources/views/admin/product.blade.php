<!DOCTYPE html>
<html lang="en">
  <head>
    @include('admin.css')
    <style type="text/css">
    .div_center
    {
        text-align: center;
        padding-top: 40px;
    }
    .font_size
    {
        font-size: 40px;
        padding-bottom: 40px;
    }
    .text_color
    {
        color: black;
        padding-bottom: 20px;
    }
    label
    {
        display:inline-block;
        width: 200px;

    }
    .div_design
    {
      padding-bottom: 15px;

    }
    </style>
  </head>
  <body>
    <div class="container-scroller">
      @include('admin.sidebar')
      <div class="container-fluid page-body-wrapper">
        @include('admin.navbar')
        <div class="main-panel">
            <div class="content-wrapper">
                @if(session()->has('message'))
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert"
                    aria-hidden="true">x</button>
                    {{session()->get('message')}}
                </div>
            @endif
                <div class="div_center">
                    <h1 class="font_size">Add Product</h1>
                    <form action="{{url('/add_product')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                    <div class="div_design">
                    <label>Product Title</label>
                    <input class="text_color" type="text" name="title" required="" placeholder="Title">
                    </div>
                    <div class="div_design">
                    <label>Description</label>
                    <input class="text_color" type="text" name="description" required="" placeholder="Description">
                    </div>
                    <div class="div_design">
                    <label>Product Price</label>
                    <input class="text_color" type="number" name="price" required="" placeholder="Price">
                    </div>
                    <div class="div_design">
                    <label>Discount Price</label>
                    <input class="text_color" type="number" name="dis_price" placeholder="Discount price">
                    </div>
                    <div class="div_design">
                    <label>Product Quantity</label>
                    <input class="text_color" min="0" type="number" name="quantity" required="" placeholder="Quantity">
                    </div>
                    <div class="div_design">
                    <label>Product Category</label>
                    <select class="text_color" name="category" required="">
                    <option value="" selected="">Select Category</option>
                    @foreach($category as $category)
                    <option value="{{$category->category_name}}">{{$category->category_name}}</option>
                    @endforeach
                    </select>
                    <div class="div_design">
                    <label>Product Image</label>
                    <input type="file" name="image" required="">
                    </div>
                    <div class="div_design">
                    <input type="submit" value="Add Product" class="btn btn-primary">
                    </div>
                </form>

                </div>
            </div>
        </div>
        </div>
      </div>
    </div>
    @include('admin.script')
  </body>
</html>