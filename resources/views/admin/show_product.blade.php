<!DOCTYPE html>
<html lang="en">
  <head>
    @include('admin.css')
    <style type="text/css">
    .center
    {
      margin: auto;
      width: 50%;
      text-align: center;
      margin-top: 30px;
      border: 3px solid white;
    }
    .h2_font
    {
        font-size: 40px;
        padding-top: 20px;
        text-align: center;
    }
    .div_center
    {
        text-align: center;
        padding-top: 40px;
    }
    .img_size
    {
        width: 100px;
        height: 100px
    }
    .th_color
    {
        background: skyblue;
    }
    .th_deg 
    {
        padding: 20px;
    }
    </style>
  </head>
  <body>
    <div class="container-scroller">
      @include('admin.sidebar')
      <div class="container-fluid page-body-wrapper">
        @include('admin.navbar')
        <div class="main-panel">
            <div class="content-wrapper">
              @if(session()->has('message'))
              <div class="alert alert-success">
                  <button type="button" class="close" data-dismiss="alert"
                  aria-hidden="true">x</button>
                  {{session()->get('message')}}
              </div>
          @endif
                <div class="div_center"></div>
                <h2 class="h2_font">All Products</h2>
                <table class="center">
                    <tr class="th_color">
                        <th class="th_deg">Title</th>
                        <th class="th_deg">Description</th>
                        <th class="th_deg">Quantity</th>
                        <th class="th_deg">Category</th>
                        <th class="th_deg">Price</th>
                        <th class="th_deg">Discount Price</th>
                        <th class="th_deg">Image</th>
                        <th class="th_deg">Delete</th>
                        <th class="th_deg">Edit</th>
                    </tr>
                    @foreach($product as $product)
                    <tr>
                        <td>{{$product->title}}</td>
                        <td>{{$product->description}}</td>
                        <td>{{$product->quantity}}</td>
                        <td>{{$product->category}}</td>
                        <td>{{$product->price}}</td>
                        <td>{{$product->discount_price}}</td>
                        <td><img class="img_size" src="/product/{{$product->image}}" alt="{{$product->title}}"></td>
                        <td><a href="{{url('delete_product', $product->id)}}" class="btn btn-danger" 
                        onclick="return confirm('Confirm to delete {{$product->title}}')">Delete</a></td>
                        <td><a href="{{url('/upd_product', $product->id)}}" class="btn btn-primary">Update</a></td>

                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
        </div>
        </div>
      </div>
    </div>
    @include('admin.script')
  </body>
</html>