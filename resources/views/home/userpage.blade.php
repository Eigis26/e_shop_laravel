<!DOCTYPE html>
<html>
   <head>
          <meta charset="utf-8" />
          <meta http-equiv="X-UA-Compatible" content="IE=edge" />
          <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
          <meta name="keywords" content="" />
          <meta name="description" content="" />
          <meta name="author" content="" />
          <link rel="shortcut icon" href="#" type="">
          <title>EShop</title>
          <link rel="stylesheet" type="text/css" href="{{asset('home/css/bootstrap.css')}}" />
          <link href="{{asset('home/css/font-awesome.min.css')}}" rel="stylesheet" />
          <link href="{{asset('home/css/style.css')}}" rel="stylesheet" />
          <link href="{{asset('home/css/responsive.css')}}" rel="stylesheet" />
    
   </head>
   <body>
      @include('sweetalert::alert')
      <div class="hero_area">
         <!-- header section strats -->
        @include('home.header')
         <!-- end header section -->
         <!-- slider section -->
         @include('home.slider')
      </div>
      <!-- why section -->
        @include('home.why')
      
      <!-- arrival section -->
      @include('home.arrival')
      
      <!-- product section -->
      @include('home.product')
      <!-- footer-->
      @include('home.footer')
      <div class="cpy_">
         <p class="mx-auto">© 2022 All Rights Reserved By $$$$$</a><br>
         </p>
      </div>
      <script src="home/js/jquery-3.4.1.min.js"></script>
      <script src="home/js/popper.min.js"></script>
      <script src="home/js/bootstrap.js"></script>
      <script src="home/js/custom.js"></script>
   </body>
</html>