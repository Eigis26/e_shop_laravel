<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
      <meta name="keywords" content="" />
      <meta name="description" content="" />
      <meta name="author" content="" />
      <link rel="shortcut icon" href="#" type="">
      <title>EShop</title>
      <link rel="stylesheet" type="text/css" href="{{asset('home/css/bootstrap.css')}}" />
      <link href="{{asset('home/css/font-awesome.min.css')}}" rel="stylesheet" />
      <link href="{{asset('home/css/style.css')}}" rel="stylesheet" />
      <link href="{{asset('home/css/responsive.css')}}" rel="stylesheet" />

      <style type="text/css">
        .center
        {
            margin: auto;
            width: 70%;
            padding: 30px;
            text-align: center;

        }
        table,th,td
        {
            border: 1px solid black;
        }
        .th_deg
        {
            padding: 10px;
            background-color: skyblue;
            font-size: 20px;
            font-weight: bold;
        }
      </style>

   </head>
   <body>
         <!-- header section-->
        @include('home.header')
         <div class="center">
            <table>
                <tr>
                    <th class="th_deg">Product Title</th>
                    <th class="th_deg">Quantity</th>
                    <th class="th_deg">Price</th>
                    <th class="th_deg">Delivery Status</th>
                    <th class="th_deg">Image</th>
                    <th class="th_deg">Cancel Order</th>
                </tr>
                @foreach($order as $order)
                <tr>
                    <td>{{$order->product_title}}</td>
                    <td>{{$order->quantity}}</td>
                    <td>{{$order->price}}</td>
                    <td>{{$order->delivery_status}}</td>
                    <td>
                        <img width="150" height="150" src="product/{{$order->image}}" alt="{{$order->product_title}}">
                    </td>
                    
                    <td>
                        @if($order->delivery_status=='Processing')
                        <a class="btn btn-danger" onclick="return confirm('Are You Sure Cancel {{$order->product_title}} ?')" href="{{url('cancel_order', $order->id)}}">X</a>
                        @elseif($order->delivery_status=='Canceled')
                        <p>Canceled</p>
                        @else
                        <p>Done</p>
                        @endif
                    </td>
                </tr>
                @endforeach
            </table>
          </div>
      <!-- footer -->
      @include('home.footer')

      <div class="cpy_">
         <p class="mx-auto">© 2022 All Rights Reserved By $$$$$</a><br>
         
         </p>
      </div>
      <script src="home/js/jquery-3.4.1.min.js"></script>
      <script src="home/js/popper.min.js"></script>
      <script src="home/js/bootstrap.js"></script>
      <script src="home/js/custom.js"></script>
   </body>
</html>