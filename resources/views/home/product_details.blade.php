<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
      <meta name="keywords" content="" />
      <meta name="description" content="" />
      <meta name="author" content="" />
      <link rel="shortcut icon" href="#" type="">
      <title>EShop</title>
      <link rel="stylesheet" type="text/css" href="{{asset('home/css/bootstrap.css')}}" />
      <link href="{{asset('home/css/font-awesome.min.css')}}" rel="stylesheet" />
      <link href="{{asset('home/css/style.css')}}" rel="stylesheet" />
      <link href="{{asset('home/css/responsive.css')}}" rel="stylesheet" />

   </head>
   <body>
      <div class="hero_area">
         <!-- header section-->
        @include('home.header')
      <div class="col-sm-6 col-md-4 col-lg-4" style="margin: auto; width: 50%;
      padding: 30px;">
           <div class="img-box" style="padding: 20px">
              <img src="/product/{{$product->image}}" alt="{{$product->title}}">
           </div>
           <div class="detail-box">
              <h5>
                 {{$product->title}}
              </h5>
              @if($product->discount_price != null)
              <h6 style="color: red">
                Discount price <br>
                ${{$product->discount_price}}
             </h6>
             <h6 style="text-decoration: line-through">
                Price <br>
                ${{$product->price}}
             </h6>
                 
             @else
             <h6>
                Price <br>
                ${{$product->price}}
             </h6> 
             @endif

             <h6>Product Category : {{$product->category}}</h6>
             <h6>Product Details : {{$product->description}}</h6>
             <h6> Available Quantity : {{$product->quantity}}</h6>
             <form action="{{url('add_cart', $product->id)}}" method="POST">
               @csrf
               <div class="row">
                  <div class="col-md-4">
                     <input style="width: 100px" type="number" name="quantity" value="1" min="1">
                  </div>
               <div class="col-md-4">
               <input type="submit" value="Add to cart">
               </div>
               </div>
             </form>
             
           </div>
        </div>
     </div>


      <!-- footer-->
      @include('home.footer')
      <div class="cpy_">
         <p class="mx-auto">© 2022 All Rights Reserved By $$$$$<br>
         
         
         </p>
      </div>
      <script src="home/js/jquery-3.4.1.min.js"></script>
      <script src="home/js/popper.min.js"></script>
      <script src="home/js/bootstrap.js"></script>
      <script src="home/js/custom.js"></script>
   </body>
</html>