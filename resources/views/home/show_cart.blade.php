<!DOCTYPE html>
<html>
   <head>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
         <meta charset="utf-8" />
         <meta http-equiv="X-UA-Compatible" content="IE=edge" />
         <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
         <meta name="keywords" content="" />
         <meta name="description" content="" />
         <meta name="author" content="" />
         <link rel="shortcut icon" href="#" type="">
         <title>EShop</title>
         <link rel="stylesheet" type="text/css" href="{{asset('home/css/bootstrap.css')}}" />
         <link href="{{asset('home/css/font-awesome.min.css')}}" rel="stylesheet" />
         <link href="{{asset('home/css/style.css')}}" rel="stylesheet" />
         <link href="{{asset('home/css/responsive.css')}}" rel="stylesheet" />
   
      <style type="text/css">
      .center
      {
        margin: auto;
        width: 50%;
        text-align: center;
        padding: 30px;
      }
      table, th, td
      {
        border: 1px solid gray;
      }
      .th_deg
      {
        font-size: 30px;
        padding: 5px;
        background: skyblue;
      }
      .img_deg
      {
        height: 150px;
        width: 150px;
      }
      .total_prc
      {
        font-size: 20px;
        padding: 40px;
      }
      </style>
   </head>
   <body>
    @include('sweetalert::alert')
      <div class="hero_area">
         <!-- header section-->
        @include('home.header')
         @if(session()->has('message'))
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert"
                    aria-hidden="true">x</button>
                    {{session()->get('message')}}
                </div>
            @endif
      <div class="center">
        
        <table>
            <tr>
                <th class="th_deg">Product Title</th>
                <th class="th_deg">Product Quantity</th>
                <th class="th_deg">Price</th>
                <th class="th_deg">Image</th>
                <th class="th_deg">Action</th>
            </tr>
            <?php $totalprice=0; ?>
            @foreach($cart as $cart)
            <tr>
                <td>{{$cart->product_title}}</td>
                <td>{{$cart->quantity}}</td>
                <td>${{$cart->price}}</td>
                <td><img class="img_deg" src="product/{{$cart->image}}" alt="{{$cart->poduct_title}}"></td>
                <td><a href="{{url('remove_cart', $cart->id)}}" onclick="confirmation(event)" class="btn btn-danger">X</a></td>
            </tr>
            <?php $totalprice=$totalprice + $cart->price ?>
            @endforeach

            
        </table>
        <div>
            <h1 class="total_prc">Total Price : ${{$totalprice}}</h1>
        </div>
        <div>
          <a href="{{url('order', $totalprice)}}" class="btn btn-danger">Cashout</a>
        </div>
      </div>
      <div class="cpy_">
         <p class="mx-auto">© 2022 All Rights Reserved By $$$$$<br>
         
         
         </p>
      </div>

      <script>
        function confirmation(event){
          event.preventDefault();
          var urlToRedirect = event.currentTarget.getAttribute('href');
          console.log(urlToRedirect);
          swal({
            title: "Are you sure to cancel this product?",
            text: "You will not be able to revert this!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willCancel) => {
            if (willCancel) {
              window.location.href = urlToRedirect;
            }
          });
        }
      </script>
      <script src="home/js/jquery-3.4.1.min.js"></script>
      <script src="home/js/popper.min.js"></script>
      <script src="home/js/bootstrap.js"></script>
      <script src="home/js/custom.js"></script>
   </body>
</html>