<footer>
    <div class="container">
       <div class="row">
          <div class="col-md-4">
              <div class="full">
                 <div class="logo_footer">
                   <a style="font-weight: bold" href="#">Logo</a>
                 </div>
                 <div class="information_f">
                   <p><strong>ADDRESS:</strong> </p>
                   <p><strong>TELEPHONE:</strong> </p>
                   <p><strong>EMAIL:</strong> </p>
                 </div>
              </div>
          </div>
          <div class="col-md-8">
             <div class="row">
             <div class="col-md-7">
                <div class="row">
                   <div class="col-md-6">
                <div class="widget_menu">
                   <h3>Menu</h3>
                   <ul>
                      <li><a href="#">Home</a></li>
                      <li><a href="#">Contact</a></li>
                   </ul>
                </div>
             </div>
             <div class="col-md-6">
                <div class="widget_menu">
                   <h3>Account</h3>
                   <ul>
                      <li><a href="#">Account</a></li>
                      <li><a href="#">Checkout</a></li>
                      <li><a href="#">Login</a></li>
                      <li><a href="#">Register</a></li>
                      <li><a href="#">Shopping</a></li>
                   </ul>
                </div>
             </div>
                </div>
             </div>     
             </div>
             </div>
          </div>
       </div>
    </div>
 </footer>